package com.morais.reginaldo.configuration;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.morais.reginaldo.configuration.properties.MongoDBConfigurationProperties;
import com.morais.reginaldo.user.model.User;
import com.morais.reginaldo.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactoryBean;

import java.util.Arrays;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MongoDBConfigurationProperties.class)
@EnableMongoRepositories(basePackages = "com.morais.reginaldo")
public class MongoRepositoryConfiguration {

    private final MongoDBConfigurationProperties mongoDBConfigurationProperties;

    @Bean
    @Primary
    @Qualifier("userRepository")
    public UserRepository userRepository(final @Qualifier("mongoTemplate") MongoTemplate template) {

        MongoRepositoryFactoryBean<UserRepository, User, UUID> factory;
        factory = new MongoRepositoryFactoryBean<>(UserRepository.class);
        factory.setMongoOperations(template);
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Bean
    @Primary
    @Qualifier("mongoTemplate")
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(getfactory(this.mongoDBConfigurationProperties.getProps()));
    }

    @Bean
    @Primary
    public MongoDbFactory getfactory(final MongoProperties mongoProperties) throws Exception {
        return createMongoDbFactory(mongoProperties);
    }

    private SimpleMongoClientDbFactory createMongoDbFactory(final MongoProperties mongoProperties) {
        return new SimpleMongoClientDbFactory(getMongoClient(), mongoProperties.getDatabase());
    }

    private MongoClient getMongoClient() {

        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(CodecRegistries.fromCodecs(
                new UuidCodec(UuidRepresentation.STANDARD)),
                MongoClientSettings.getDefaultCodecRegistry());

        return MongoClients.create(MongoClientSettings.builder().applyToClusterSettings(builder ->
                builder.hosts(Arrays.asList(new ServerAddress()))).codecRegistry(codecRegistry).build());
    }
}

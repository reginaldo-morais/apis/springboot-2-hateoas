package com.morais.reginaldo.health;

import com.morais.reginaldo.health.service.HealthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "health", produces = {"application/json;charset=UTF-8"})
public class HealthController {

    @Autowired
    HealthService healthService;

    @GetMapping
    public ResponseEntity<Health> get() {

        Health health = healthService.getHealth();

        if (health.getStatus() == Status.DOWN) {
            return new ResponseEntity<>(health, HttpStatus.SERVICE_UNAVAILABLE);
        }

        return new ResponseEntity<>(health, HttpStatus.OK);
    }
}

package com.morais.reginaldo.health.service;

import com.morais.reginaldo.health.check.MongoTemplateHealthIndicator;
import com.morais.reginaldo.health.check.RestTemplateHealthIndicator;
import com.morais.reginaldo.health.properties.HealthConfigurationProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@AllArgsConstructor
@EnableConfigurationProperties(HealthConfigurationProperties.class)
@Service
public class HealthServiceImpl implements HealthService {

    private MongoTemplateHealthIndicator mongoTemplateHealthIndicator;

    private RestTemplateHealthIndicator restTemplateHealthIndicator;

    private HealthConfigurationProperties healthConfigurationProperties;

    @Override
    public Health getHealth() {

        Health.Builder builder = new Health.Builder();
        Health health = getHealthWithDetails();

        if (healthConfigurationProperties.isShowDetails()) {
            return health;
        }

        if (health.getStatus() == Status.DOWN) {
            return builder.status(Status.DOWN).build();
        }

        return builder.status(Status.UP).build();
    }

    @Override
    public Health getHealthWithDetails() {

        Health.Builder builder = new Health.Builder();
        Map<String, Health> mongoDetails = new HashMap<>();

        Health mongoHealth =  mongoTemplateHealthIndicator.health();
        Health gitRestTemplateHealth =  restTemplateHealthIndicator.health();

        mongoDetails.put("mongodb", mongoHealth);
        mongoDetails.put("git-rest-template", gitRestTemplateHealth);

        if (mongoHealth.getStatus() == Status.DOWN || gitRestTemplateHealth.getStatus() == Status.DOWN) {
            return builder.withDetails(mongoDetails).status(Status.DOWN).build();
        }

        return builder.withDetails(mongoDetails).status(Status.UP).build();
    }
}

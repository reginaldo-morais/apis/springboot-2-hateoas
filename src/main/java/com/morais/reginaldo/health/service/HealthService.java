package com.morais.reginaldo.health.service;

import org.springframework.boot.actuate.health.Health;

public interface HealthService {

    Health getHealth();

    Health getHealthWithDetails();
}

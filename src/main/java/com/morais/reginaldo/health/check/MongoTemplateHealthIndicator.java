package com.morais.reginaldo.health.check;

import lombok.AllArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.mongo.MongoHealthIndicator;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class MongoTemplateHealthIndicator implements HealthIndicator {

    private MongoTemplate mongoTemplate;

    @Override
    public Health health() {

        MongoHealthIndicator mongoHealthIndicator = new MongoHealthIndicator(mongoTemplate);
        Health health = mongoHealthIndicator.getHealth(true);
        return health;
    }
}

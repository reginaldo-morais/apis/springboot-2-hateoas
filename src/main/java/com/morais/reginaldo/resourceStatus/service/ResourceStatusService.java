package com.morais.reginaldo.resourceStatus.service;

import com.morais.reginaldo.resourceStatus.representation.ResourceStatusResponse;

public interface ResourceStatusService {

    ResourceStatusResponse get();
}

package com.morais.reginaldo.resourceStatus;

import com.morais.reginaldo.resourceStatus.representation.ResourceStatusResponse;
import com.morais.reginaldo.resourceStatus.service.ResourceStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "resource-status", produces = {"application/json;charset=UTF-8"})
public class ResourceStatusController {

    @Autowired
    ResourceStatusService resourceStatusService;

    @GetMapping
    public ResponseEntity<ResourceStatusResponse> get() throws Exception {

        return new ResponseEntity<>(resourceStatusService.get(), HttpStatus.OK);
    }
}

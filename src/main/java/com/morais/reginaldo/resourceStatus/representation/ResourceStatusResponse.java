package com.morais.reginaldo.resourceStatus.representation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceStatusResponse {

    private String createdBy;

    private String buildJDK;

    private String applicationName;

    private String implementationBuild;

    private String implementationVersion;
}

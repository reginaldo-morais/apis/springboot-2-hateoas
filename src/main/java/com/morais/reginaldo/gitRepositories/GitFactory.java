package com.morais.reginaldo.gitRepositories;

import com.morais.reginaldo.gitRepositories.connector.GitConnector;
import com.morais.reginaldo.user.enumeration.Git;
import com.morais.reginaldo.gitRepositories.connector.github.GithubConnector;
import com.morais.reginaldo.gitRepositories.exception.GitConnectorMappingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@AllArgsConstructor
@Component
public class GitFactory {

    private GithubConnector githubConnector;

    public GitConnector createConnector(final Git git) {

        log.info("Returning connector by {}", git);

        switch (git) {
            case GITHUB:
                log.info("GithubConnector founded");
                return githubConnector;
            default:
                throw new GitConnectorMappingException("Any Git Repository Mapping founded");
        }
    }
}

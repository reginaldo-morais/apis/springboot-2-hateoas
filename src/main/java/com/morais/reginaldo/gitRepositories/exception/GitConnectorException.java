package com.morais.reginaldo.gitRepositories.exception;

public class GitConnectorException extends RuntimeException {

    public GitConnectorException() { }

    public GitConnectorException(final String message) {
        super(message);
    }

    public GitConnectorException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}

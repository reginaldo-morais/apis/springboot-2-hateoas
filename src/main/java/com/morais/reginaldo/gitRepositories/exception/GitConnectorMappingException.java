package com.morais.reginaldo.gitRepositories.exception;

public class GitConnectorMappingException extends RuntimeException {

    public GitConnectorMappingException(final String message) {
        super(message);
    }

    public GitConnectorMappingException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
package com.morais.reginaldo.gitRepositories.connector.github.representation;

import com.morais.reginaldo.gitRepositories.representation.GitResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GithubResponse implements GitResponse {

    private List<GithubRepository> githubRepositories;
}

package com.morais.reginaldo.gitRepositories.connector.github;

import com.morais.reginaldo.gitRepositories.connector.GitConnector;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubRepository;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubResponse;
import com.morais.reginaldo.gitRepositories.exception.GitConnectorException;
import com.morais.reginaldo.gitRepositories.properties.GitRepositoriesConfigurationProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Slf4j
@AllArgsConstructor
@EnableConfigurationProperties(GitRepositoriesConfigurationProperties.class)
@Service
public class GithubConnector implements GitConnector {

    private RestTemplate restTemplate;

    private GitRepositoriesConfigurationProperties configurationProperties;

    @Override
    public GithubResponse get(final String gitUsername) {

        try {
            log.info("Getting user repos from Github");

            final String url = configurationProperties.getUrl() + gitUsername + "/repos";
            final HttpEntity requestEntity = new HttpEntity(getHeader());

            final ResponseEntity<GithubRepository[]> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    GithubRepository[].class);

            return new GithubResponse(new ArrayList<>(Arrays.asList(response.getBody())));

        } catch (Exception e) {
            log.error("Error to get user repos from Github", e);
            throw new GitConnectorException("Error to get user repos from Github", e);
        }
    }

    private HttpHeaders getHeader() {

        final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return header;
    }
}

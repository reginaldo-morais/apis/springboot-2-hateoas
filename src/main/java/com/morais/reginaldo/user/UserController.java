package com.morais.reginaldo.user;

import com.morais.reginaldo.user.representation.ErrorMessageResponse;
import com.morais.reginaldo.user.representation.UserRequest;
import com.morais.reginaldo.user.representation.UserResponse;
import com.morais.reginaldo.user.representation.UserPatchRequest;
import com.morais.reginaldo.user.service.UserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@Api(tags = "Users", value = "users", description = "Resource of Users")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorMessageResponse.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorMessageResponse.class)
})
@Slf4j
@RestController
@RequestMapping(value = "users", produces = {"application/json;charset=UTF-8"})
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Create a new user")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<EntityModel<UserResponse>> post(@RequestBody @Validated final UserRequest userRequest) {

        log.info("POST User {}", userRequest);

        UserResponse userResponse = userService.create(userRequest);
        return new ResponseEntity<>(new EntityModel<>(userResponse), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessageResponse.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/{id}")
    public ResponseEntity<EntityModel<UserResponse>> put(
            @ApiParam(value = "Id of a User", required = true) @PathVariable final UUID id,
            @RequestBody @Validated final UserRequest userRequest) {

        log.info("PUT User {} {}", id.toString(), userRequest);

        UserResponse userResponse = userService.update(id, userRequest);

        if (userResponse.isNewUser()) {
            return new ResponseEntity<>(new EntityModel<>(userResponse), HttpStatus.CREATED);
        }

        return new ResponseEntity<>(new EntityModel<>(userResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Update some attributes of a user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessageResponse.class)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(path = "/{id}")
    public ResponseEntity<?> patch(
            @ApiParam(value = "Id of a User", required = true) @PathVariable final UUID id,
            @RequestBody @Validated final UserPatchRequest userPatchRequest) {

        log.info("PATCH User {} {}", id.toString(), userPatchRequest);

        if (!userPatchRequest.hasSomeAttribute()) {
            return new ResponseEntity<>(
                    new ErrorMessageResponse("UserPatch Request not be null"),
                    HttpStatus.BAD_REQUEST);
        }

        userService.patch(id, userPatchRequest);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "Get a user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessageResponse.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}")
    public ResponseEntity<EntityModel<UserResponse>> get(
            @ApiParam(value = "Id of a User", required = true) @PathVariable final UUID id) {

        log.info("GET User {}", id.toString());

        UserResponse userResponse = userService.get(id);
        return new ResponseEntity<>(new EntityModel<>(userResponse), HttpStatus.OK);
    }

    @ApiOperation(value = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found")
    })
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "page",
                    defaultValue = "0",
                    dataType = "integer",
                    paramType = "query",
                    value = "Número da pagina da busca"),
            @ApiImplicitParam(
                    name = "size",
                    defaultValue = "20",
                    dataType = "integer",
                    paramType = "query",
                    value = "Tamanho da página da busca"),
            @ApiImplicitParam(
                    name = "direction",
                    allowableValues = "ASC,DESC",
                    defaultValue = "DESC",
                    dataType = "string",
                    paramType = "query",
                    value = "Direção da ordenação da busca")
    })
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public ResponseEntity<PagedModel<EntityModel<UserResponse>>> list(
            @ApiIgnore("Ignored because swagger ui shows the wrong params, "
                    + "instead they are explained in the implicit params")
            @PageableDefault(page = 0,
                    size = 20,
                    sort = "createdAt",
                    direction = Sort.Direction.DESC) final Pageable pageable,
            final PagedResourcesAssembler<UserResponse> pagedResourcesAssembler) {

        log.info("LIST User");

        Page<UserResponse> userResponsePage = userService.list(pageable);
        return new ResponseEntity<>(
                pagedResourcesAssembler.toModel(userResponsePage, this.getSelfLink()),
                HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessageResponse.class)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@ApiParam(value = "Id of a User", required = true)
                                    @PathVariable final UUID id) {

        log.info("DELETE User {}", id.toString());

        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Return a link of resource from Controller.
     *
     * @return link
     */
    private Link getSelfLink() {

        return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build()
                .toUriString())
                .withSelfRel();
    }
}

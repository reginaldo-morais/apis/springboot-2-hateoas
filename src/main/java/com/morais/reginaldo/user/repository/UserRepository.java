package com.morais.reginaldo.user.repository;

import com.morais.reginaldo.user.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;
import java.util.UUID;

@NoRepositoryBean
public interface UserRepository extends MongoRepository<User, UUID> {

    Optional<User> findById(UUID id);
}

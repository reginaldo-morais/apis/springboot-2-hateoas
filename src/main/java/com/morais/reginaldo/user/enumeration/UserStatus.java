package com.morais.reginaldo.user.enumeration;

import com.morais.reginaldo.user.exception.UserStatusNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@AllArgsConstructor
public enum UserStatus {

    INACTIVE(0),
    ACTIVE(1);

    private final int code;

    public static List<Integer> getCodes() {

        List<Integer> userStatusList = new ArrayList<>();

        for (UserStatus userStatus : UserStatus.values()) {
            userStatusList.add(userStatus.getCode());
        }

        return userStatusList;
    }

    public static UserStatus findByName(final String name) {

        for (UserStatus userStatus : UserStatus.values()) {
            if (userStatus.name().equalsIgnoreCase(name)) {
                return userStatus;
            }
        }

        log.error("Nao foi possivel determinar o status do Usuário para o valor [{}]", name);
        throw new UserStatusNotFoundException();
    }

    public static UserStatus findByCode(final int code) {

        for (UserStatus userStatus : UserStatus.values()) {
            if (userStatus.code == code) {
                return userStatus;
            }
        }

        log.error("Nao foi possivel determinar o status do Usuário para o código [{}]", code);
        throw new UserStatusNotFoundException();
    }
}

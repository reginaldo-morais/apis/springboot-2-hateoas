package com.morais.reginaldo.user.representation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.morais.reginaldo.user.enumeration.UserStatus;
import com.morais.reginaldo.user.model.GitRepository;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(description = "Payload of User")
public class UserResponse extends RepresentationModel<UserResponse> {

    @ApiModelProperty(value = "Id of User", required = true, example = "cb29d4fc-f9e5-11e9-8f0b-362b9e155667")
    private UUID id;

    @ApiModelProperty(value = "Name of User. Min of 2", required = true, example = "John Doe")
    private String name;

    @ApiModelProperty(value = "E-mail of User", required = true, example = "john.doe@example.com")
    private String email;

    @ApiModelProperty(value = "List of git repositories")
    private List<GitRepository> gitRepositories;

    @ApiModelProperty(value = "Status active of User", required = true, example = "true")
    private UserStatus status;

    @ApiModelProperty(value = "Created date of User", required = true, example = "2019-08-04T22:01:32.348Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = "UTC")
    private Instant createdAt;

    @ApiModelProperty(value = "Updated date of User", required = true, example = "2019-08-04T22:01:32.348Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = "UTC")
    private Instant updatedAt;

    @JsonIgnore
    private boolean newUser;
}

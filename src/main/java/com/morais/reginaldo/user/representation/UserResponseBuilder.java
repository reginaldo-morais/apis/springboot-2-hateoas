package com.morais.reginaldo.user.representation;

import com.morais.reginaldo.user.UserController;
import com.morais.reginaldo.user.enumeration.UserStatus;
import com.morais.reginaldo.user.model.GitRepository;
import com.morais.reginaldo.user.model.User;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class UserResponseBuilder {

    private UUID id;

    private String name;

    private String email;

    private List<GitRepository> gitRepositories;

    private UserStatus status;

    private Instant createdAt;

    private Instant updatedAt;

    private List<Link> resourceLinkList = new ArrayList<>();

    private boolean newUser;

    public static UserResponseBuilder anUserResponse() {
        return new UserResponseBuilder();
    }

    public static UserResponseBuilder anUserResponse(final User user) {
        return new UserResponseBuilder(user);
    }

    public UserResponseBuilder(final User user) {

        this.withId(user.getId())
                .withName(user.getName())
                .withEmail(user.getEmail())
                .withGitRepository(user.getGitRepositories())
                .withStatus(user.getStatus())
                .withCreatedAt(user.getCreatedAt())
                .withUpdatedAt(user.getUpdatedAt())
                .withLink(WebMvcLinkBuilder.linkTo(UserController.class).slash(user.getId()).withSelfRel());
    }

    public UserResponseBuilder withId(final UUID id) {
        this.id = id;
        return this;
    }

    public UserResponseBuilder withName(final String name) {
        this.name = name;
        return this;
    }

    public UserResponseBuilder withEmail(final String email) {
        this.email = email;
        return this;
    }

    public UserResponseBuilder withGitRepository(final List<GitRepository> gitRepositories) {
        this.gitRepositories = gitRepositories;
        return this;
    }

    public UserResponseBuilder withStatus(final UserStatus active) {
        this.status = active;
        return this;
    }

    public UserResponseBuilder withCreatedAt(final Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public UserResponseBuilder withUpdatedAt(final Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public UserResponseBuilder withLink(final Link link) {
        this.resourceLinkList.add(link);
        return this;
    }

    public UserResponseBuilder withNewUser(final boolean newUser) {
        this.newUser = newUser;
        return this;
    }

    public UserResponse build() {

        UserResponse userResponse = new UserResponse();
        userResponse.setId(id);
        userResponse.setName(name);
        userResponse.setEmail(email);
        userResponse.setGitRepositories(gitRepositories);
        userResponse.setStatus(status);
        userResponse.setCreatedAt(createdAt);
        userResponse.setUpdatedAt(updatedAt);
        if (!resourceLinkList.isEmpty()) {
            userResponse.add(resourceLinkList);
        }
        userResponse.setNewUser(newUser);

        return userResponse;
    }
}

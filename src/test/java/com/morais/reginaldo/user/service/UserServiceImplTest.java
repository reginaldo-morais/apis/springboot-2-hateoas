package com.morais.reginaldo.user.service;

import com.morais.reginaldo.gitRepositories.GitService;
import com.morais.reginaldo.user.enumeration.UserStatus;
import com.morais.reginaldo.user.exception.UserNotFoundException;
import com.morais.reginaldo.user.model.User;
import com.morais.reginaldo.user.repository.UserRepository;
import com.morais.reginaldo.user.representation.UserPatchRequest;
import com.morais.reginaldo.user.representation.UserRequest;
import com.morais.reginaldo.user.representation.UserResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private GitService gitService;

    private UserService userService;

    private User user;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);

        userService = new UserServiceImpl(userRepository, gitService);
        user = new User(UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c"),
                "John Doe",
                "john.doe@example.com",
                UserStatus.ACTIVE,
                new ArrayList<>(),
                Instant.now(),
                Instant.now());
    }

    @Test
    public void test_post_user_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
        final String name = "John Doe";
        final String email = "john.doe@example.com";

        final UserRequest userRequest = new UserRequest();
        userRequest.setName(name);

        when(userRepository.insert(ArgumentMatchers.any(User.class))).thenReturn(user);

        final UserResponse userResponse = userService.create(userRequest);

        assertEquals(id, userResponse.getId());
        assertEquals(name, userResponse.getName());
        assertEquals(email, userResponse.getEmail());
    }

    @Test
    public void test_put_user_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
        final String name = "John Doe";
        final String email = "john.doe@example.com";

        final UserRequest userRequest = new UserRequest();
        userRequest.setName(name);
        userRequest.setEmail(email);
        userRequest.setStatus(UserStatus.INACTIVE);

        user.setStatus(UserStatus.INACTIVE);

        when(userRepository.findById(id)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);

        final UserResponse userResponse = userService.update(id, userRequest);

        assertEquals(UserStatus.INACTIVE, userResponse.getStatus());
        assertEquals(id, userResponse.getId());
        assertEquals(name, userResponse.getName());
        assertEquals(email, userResponse.getEmail());
    }

    @Test
    public void test_put_user_not_found_should_save_and_return_success() throws Exception {

        final UUID id = UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f");
        final String name = "John Doe 2";
        final String email = "john.doe2@example.com";

        User user2 = new User(id, name, email, UserStatus.ACTIVE, new ArrayList<>(), Instant.now(), Instant.now());

        final UserRequest userRequest = new UserRequest();
        userRequest.setName(name);
        userRequest.setEmail(email);
        userRequest.setStatus(UserStatus.INACTIVE);

        user.setStatus(UserStatus.INACTIVE);

        when(userRepository.findById(id)).thenReturn(Optional.empty());
        when(userRepository.insert(ArgumentMatchers.any(User.class))).thenReturn(user2);

        final UserResponse userResponse = userService.update(id, userRequest);

        assertEquals(UserStatus.ACTIVE, userResponse.getStatus());
        assertEquals(id, userResponse.getId());
        assertEquals(name, userResponse.getName());
        assertEquals(email, userResponse.getEmail());
    }

    @Test
    public void test_patch_user_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
        final UserPatchRequest userPatchRequest = new UserPatchRequest();
        userPatchRequest.setName("Jorge Jackson");
        userPatchRequest.setEmail("jorge.jackson@example.com");
        userPatchRequest.setGitUsername("jorge.jackson");
        userPatchRequest.setStatus(UserStatus.INACTIVE);

        user.setStatus(UserStatus.INACTIVE);

        when(userRepository.findById(id)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);

        userService.patch(id, userPatchRequest);
    }

    @Test
    public void test_patch_user_without_attributes_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
        final UserPatchRequest userPatchRequest = new UserPatchRequest();

        when(userRepository.findById(id)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);

        userService.patch(id, userPatchRequest);
    }

    @Test
    public void test_patch_not_founded_user_should_return_not_found() throws Exception {

        assertThrows(UserNotFoundException.class, () -> {
            final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
            final UserPatchRequest userPatchRequest = new UserPatchRequest();
            userPatchRequest.setName("Jorge Jackson");
            userPatchRequest.setEmail("jorge.jackson@example.com");
            userPatchRequest.setStatus(UserStatus.INACTIVE);

            when(userRepository.findById(id)).thenReturn(Optional.empty());

            userService.patch(id, userPatchRequest);
        });
    }

    @Test
    public void test_list_user_should_return_success() throws Exception {

        User user2 = new User(
                UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f"),
                "John Doe 2",
                "john.doe2@example.com",
                UserStatus.ACTIVE,
                new ArrayList<>(),
                Instant.now(),
                Instant.now());

        List<User> userList = new ArrayList<>();
        userList.add(user);
        userList.add(user2);

        Pageable pageable = PageRequest.of(1, 2, Sort.Direction.ASC, "id");
        Page<User> userPage = new PageImpl<>(userList);

        when(userRepository.findAll(pageable)).thenReturn(userPage);

        final Page<UserResponse> userPageResponse = userService.list(pageable);

        assertTrue(userPageResponse.getTotalElements() == 2);
    }

    @Test
    public void test_list_user_should_return_not_found() throws Exception {

        assertThrows(UserNotFoundException.class, () -> {
            Pageable pageable = PageRequest.of(1, 2, Sort.by(Sort.Direction.ASC, "id"));
            when(userRepository.findAll(pageable)).thenReturn(null);
            userService.list(pageable);
        });
    }

    @Test
    public void test_get_user_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");
        final String name = "John Doe";

        when(userRepository.findById(id)).thenReturn(Optional.ofNullable(user));

        final UserResponse userResponse = userService.get(id);

        assertTrue(userResponse != null);
        assertEquals(id, userResponse.getId());
        assertEquals(name, userResponse.getName());
    }

    @Test
    public void test_get_user_should_return_not_found() throws Exception {

        assertThrows(UserNotFoundException.class, () -> {
            final UUID id = UUID.fromString("cfba8610-9f00-42ba-9a72-134ccca6ba79");

            when(userRepository.findById(id)).thenReturn(Optional.empty());
            userService.get(id);
        });
    }

    @Test
    public void test_delete_user_should_return_success() throws Exception {

        final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");

        when(userRepository.findById(id)).thenReturn(Optional.ofNullable(user));
        doNothing().when(userRepository).delete(user);

        userService.delete(id);
    }

    @Test
    public void test_delete_not_founded_user_should_return_not_found() throws Exception {

        assertThrows(UserNotFoundException.class, () -> {
            final UUID id = UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c");

            when(userRepository.findById(id)).thenReturn(Optional.empty());

            userService.delete(id);
        });
    }
}

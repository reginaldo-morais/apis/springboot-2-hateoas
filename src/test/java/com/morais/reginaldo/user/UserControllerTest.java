package com.morais.reginaldo.user;

import com.morais.reginaldo.user.enumeration.UserStatus;
import com.morais.reginaldo.user.exception.UserNotFoundException;
import com.morais.reginaldo.user.model.User;
import com.morais.reginaldo.user.representation.UserResponse;
import com.morais.reginaldo.user.representation.UserResponseBuilder;
import com.morais.reginaldo.user.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
@EnableSpringDataWebSupport
public class UserControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private User user;

    @BeforeEach
    public void Before() {

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();

        user = new User(UUID.fromString("479747e4-eeaf-4dbf-8452-a0a14c58393c"),
                "John Doe",
                "john.doe@example.com",
                UserStatus.ACTIVE,
                new ArrayList<>(),
                Instant.now(),
                Instant.now());
    }

    @Test
    public void test_post_user_should_return_created() throws Exception {

        final String json = "{\n" +
                "  \"name\": \"John Doe\",\n" +
                "  \"email\": \"john.doe@example.com\",\n" +
                "  \"gitUsername\": \"john.doe\",\n" +
                "  \"status\": \"ACTIVE\"\n" +
                "}";

        UserResponse userResponse = UserResponseBuilder.anUserResponse(user).build();

        when(userService.create(ArgumentMatchers.any())).thenReturn(userResponse);

        final ResultActions resultActions = mockMvc.perform(
                post("/users")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("479747e4-eeaf-4dbf-8452-a0a14c58393c"))
                .andExpect(jsonPath("$.name").value("John Doe"));
    }

    @Test
    public void test_post_user_should_return_bad_request() throws Exception {

        final String json = "{\n" +
                "  \"status\": \"status\"\n" +
                "}";

        UserResponse userResponse = UserResponseBuilder.anUserResponse(user).build();

        when(userService.create(ArgumentMatchers.any())).thenReturn(userResponse);

        final ResultActions resultActions = mockMvc.perform(post("/users")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void test_put_user_should_return_success() throws Exception {

        final String json = "{\n" +
                "  \"name\": \"John Doe\",\n" +
                "  \"email\": \"john.doe@example.com\",\n" +
                "  \"gitUsername\": \"john.doe\",\n" +
                "  \"status\": \"INACTIVE\"\n" +
                "}";

        user.setStatus(UserStatus.INACTIVE);

        UserResponse userResponse = UserResponseBuilder.anUserResponse(user).withNewUser(false).build();

        when(userService.update(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(userResponse);

        final ResultActions resultActions = mockMvc.perform(
                put("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("479747e4-eeaf-4dbf-8452-a0a14c58393c"))
                .andExpect(jsonPath("$.name").value("John Doe"))
                .andExpect(jsonPath("$.status").value("INACTIVE"));
    }

    @Test
    public void test_put_user_should_return_created() throws Exception {

        final String json = "{\n" +
                "  \"name\": \"John Doe\",\n" +
                "  \"email\": \"john.doe@example.com\",\n" +
                "  \"gitUsername\": \"john.doe\",\n" +
                "  \"status\": \"INACTIVE\"\n" +
                "}";

        user.setStatus(UserStatus.INACTIVE);

        UserResponse userResponse = UserResponseBuilder.anUserResponse(user).withNewUser(true).build();

        when(userService.update(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(userResponse);

        final ResultActions resultActions = mockMvc.perform(
                put("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("479747e4-eeaf-4dbf-8452-a0a14c58393c"))
                .andExpect(jsonPath("$.name").value("John Doe"))
                .andExpect(jsonPath("$.status").value("INACTIVE"));
    }

    @Test
    public void test_patch_user_should_return_success() throws Exception {

        final String json = "{\n" +
                "  \"name\": \"John Doe\",\n" +
                "  \"status\": \"INACTIVE\"\n" +
                "}";

        doNothing().when(userService).patch(any(), any());

        final ResultActions resultActions = mockMvc.perform(
                patch("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isNoContent());
    }

    @Test
    public void test_patch_user_without_attributes_should_return_bad_request() throws Exception {

        final String json = "{}";

        doNothing().when(userService).patch(any(), any());

        final ResultActions resultActions = mockMvc.perform(
                patch("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void test_patch_not_founded_user_should_return_not_found() throws Exception {

        final UUID id = UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f");
        final String json = "{\n" +
                "  \"name\": \"John Doe\",\n" +
                "  \"email\": \"john.doe@example.com\",\n" +
                "  \"gitUsername\": \"john.doe\",\n" +
                "  \"status\": \"INACTIVE\"\n" +
                "}";

        doThrow(UserNotFoundException.class).when(userService).patch(any(), any());

        final ResultActions resultActions = mockMvc.perform(
                patch("/users/{id}", "92b86dc7-c90c-431a-a139-03c6f32b8d8f")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void test_list_user_should_return_success() throws Exception {

        User user2 = new User(
                UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f"),
                "John Doe 2",
                "john.doe2@example.com",
                UserStatus.ACTIVE,
                new ArrayList<>(),
                Instant.now(),
                Instant.now());

        List<User> userList = new ArrayList<>();
        userList.add(user);
        userList.add(user2);

        Page<User> userPage = new PageImpl<>(userList);

        Page<UserResponse> userResponsePage = userPage.map(u -> UserResponseBuilder.anUserResponse(u).build());

        when(userService.list(ArgumentMatchers.any(Pageable.class))).thenReturn(userResponsePage);

        final ResultActions resultActions = mockMvc.perform(get("/users"));

        resultActions.andExpect(status().isOk());
    }

    @Test
    public void test_list_user_should_return_not_found() throws Exception {

        when(userService.list(ArgumentMatchers.any(Pageable.class))).thenThrow(new UserNotFoundException());

        final ResultActions resultActions = mockMvc.perform(get("/users"));

        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void test_get_user_should_return_success() throws Exception {

        UserResponse userResponse = UserResponseBuilder.anUserResponse(user).build();

        when(userService.get(ArgumentMatchers.any())).thenReturn(userResponse);

        final ResultActions resultActions = mockMvc.perform(
                get("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c"));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("479747e4-eeaf-4dbf-8452-a0a14c58393c"))
                .andExpect(jsonPath("$.name").value("John Doe"));
    }

    @Test
    public void test_get_user_should_return_not_found() throws Exception {

        when(userService.get(ArgumentMatchers.any())).thenThrow(new UserNotFoundException());

        final ResultActions resultActions = mockMvc.perform(
                get("/users/{id}", "479747e4-eeaf-4dbf-8452-a0a14c58393c")
                        .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void test_delete_user_should_return_no_content() throws Exception {

        final UUID id = UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f");

        doNothing().when(userService).delete(id);

        final ResultActions resultActions = mockMvc.perform(
                delete("/users/{id}", id));

        resultActions.andExpect(status().isNoContent());
    }

    @Test
    public void test_delete_not_founded_user_should_return_not_found() throws Exception {

        final UUID id = UUID.fromString("92b86dc7-c90c-431a-a139-03c6f32b8d8f");

        doThrow(UserNotFoundException.class).when(userService).delete(id);

        final ResultActions resultActions = mockMvc.perform(
                delete("/users/{id}", id));

        resultActions.andExpect(status().isNotFound());
    }
}

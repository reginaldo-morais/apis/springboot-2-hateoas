package com.morais.reginaldo.user.enumeration;

import com.morais.reginaldo.user.exception.GitNotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GitTest {

    @Test
    public void test_get_git_enum_from_string_should_return_success() throws Exception {

        Git git = Git.fromString("GITHUB");
        assertEquals(Git.GITHUB, git);
    }

    @Test
    public void test_get_git_enum_from_wrong_string_should_return_exception() throws Exception {

        assertThrows(GitNotFoundException.class, () -> {
            Git.fromString("SOMEGIT");
        });
    }
}

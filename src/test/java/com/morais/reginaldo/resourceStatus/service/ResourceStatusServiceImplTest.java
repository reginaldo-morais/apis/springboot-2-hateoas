package com.morais.reginaldo.resourceStatus.service;

import com.morais.reginaldo.resourceStatus.properties.AppManifestConfigurationProperties;
import com.morais.reginaldo.resourceStatus.representation.ResourceStatusResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ResourceStatusServiceImplTest {

    @Mock
    private AppManifestConfigurationProperties configurationProperties;

    private ResourceStatusService resourceStatusService;

    private ResourceStatusResponse resourceStatusResponse;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        resourceStatusService = new ResourceStatusServiceImpl(configurationProperties);

        resourceStatusResponse = new ResourceStatusResponse(
                "Gradle Version 6.2.2",
                "13.0.2 (Azul Systems, Inc. 13.0.2+6-MTS)",
                "spring-hateoas",
                "20200402",
                "2.1");
    }

    @Test
    public void test_get_resource_status_should_return_success() throws Exception {

        when(configurationProperties.getCreatedBy())
                .thenReturn(resourceStatusResponse.getCreatedBy());
        when(configurationProperties.getBuildJDK())
                .thenReturn(resourceStatusResponse.getBuildJDK());
        when(configurationProperties.getApplicationName())
                .thenReturn(resourceStatusResponse.getApplicationName());
        when(configurationProperties.getImplementationBuild())
                .thenReturn(resourceStatusResponse.getImplementationBuild());
        when(configurationProperties.getImplementationVersion())
                .thenReturn(resourceStatusResponse.getImplementationVersion());

        ResourceStatusResponse resourceStatusResponseResult = resourceStatusService.get();

        assertEquals(resourceStatusResponse, resourceStatusResponseResult);
    }
}
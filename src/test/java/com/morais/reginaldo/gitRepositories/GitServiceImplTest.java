package com.morais.reginaldo.gitRepositories;

import com.morais.reginaldo.gitRepositories.connector.github.GithubConnector;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubRepository;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubResponse;
import com.morais.reginaldo.gitRepositories.properties.GitRepositoriesConfigurationProperties;
import com.morais.reginaldo.user.enumeration.Git;
import com.morais.reginaldo.user.model.GitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class GitServiceImplTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private GitRepositoriesConfigurationProperties configurationProperties;

    @Mock
    private GithubConnector githubConnector;

    @Mock
    private GitFactory gitFactory;

    private GitService gitService;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        gitService = new GitServiceImpl(gitFactory);
    }

    @Test
    public void test_get_git_hub_repositories_by_git_username_should_return_list_of_git_repositories()
            throws Exception {

        final String gitUsername = "john.doe";

        mockGetByGithub(gitUsername);

        List<GitRepository> gitRepositories = gitService.getByGithub(gitUsername);

        assertEquals(2, gitRepositories.size());
        assertEquals(Git.GITHUB, gitRepositories.get(0).getGit());
    }

    @Test
    public void test_get_all_git_repositories_by_git_username_should_return_list_of_git_repositories()
            throws Exception {

        final String gitUsername = "john.doe";

        mockGetByGithub(gitUsername);

        List<GitRepository> gitRepositories = gitService.getAll(gitUsername);

        assertEquals(2, gitRepositories.size());
        assertEquals(Git.GITHUB, gitRepositories.get(0).getGit());
    }

    @Test
    public void test_get_all_git_repositories_by_git_username_without_repositories_should_return_empty_list()
            throws Exception {

        final String gitUsername = "john.doe";

        GithubResponse githubResponse = new GithubResponse(new ArrayList<>());
        when(gitFactory.createConnector(Git.GITHUB)).thenReturn(githubConnector);
        when(githubConnector.get(gitUsername)).thenReturn(githubResponse);

        List<GitRepository> gitRepositories = gitService.getAll(gitUsername);

        assertEquals(0, gitRepositories.size());
    }

    private void mockGetByGithub(final String gitUsername) {

        GithubResponse githubResponse = new GithubResponse(buildGithubRepositories());
        when(gitFactory.createConnector(Git.GITHUB)).thenReturn(githubConnector);
        when(githubConnector.get(gitUsername)).thenReturn(githubResponse);
    }

    private List<GithubRepository> buildGithubRepositories() {

        GithubRepository githubRepository = new GithubRepository(1, "some-uri", "repo-name", "some-description", false);
        GithubRepository githubRepositoryTwo = new GithubRepository(1, "some-uri", "repo-name", "some-description", false);

        List<GithubRepository> githubRepositories = new ArrayList<>();
        githubRepositories.add(githubRepository);
        githubRepositories.add(githubRepositoryTwo);

        return githubRepositories;
    }
}

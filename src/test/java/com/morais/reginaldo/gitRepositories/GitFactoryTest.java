package com.morais.reginaldo.gitRepositories;

import com.morais.reginaldo.gitRepositories.connector.GitConnector;
import com.morais.reginaldo.gitRepositories.connector.github.GithubConnector;
import com.morais.reginaldo.gitRepositories.exception.GitConnectorMappingException;
import com.morais.reginaldo.gitRepositories.properties.GitRepositoriesConfigurationProperties;
import com.morais.reginaldo.user.enumeration.Git;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
public class GitFactoryTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private GitRepositoriesConfigurationProperties configurationProperties;

    private GithubConnector githubConnector;

    private GitFactory gitFactory;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        githubConnector = new GithubConnector(restTemplate, configurationProperties);
        gitFactory = new GitFactory(githubConnector);
    }

    @Test
    public void test_create_git_connector_by_git_enum_github_should_return_github_connector() throws Exception {

        GitConnector gitConnector = gitFactory.createConnector(Git.GITHUB);
        assertEquals(GithubConnector.class, gitConnector.getClass());
    }

    @Test
    public void test_create_git_connector_by_git_enum_not_mapped_should_return_exception() throws Exception {

        assertThrows(GitConnectorMappingException.class, () -> {
            GitConnector gitConnector = gitFactory.createConnector(Git.ATLASIAN_STASH);
            assertEquals(GithubConnector.class, gitConnector.getClass());
        });
    }
}

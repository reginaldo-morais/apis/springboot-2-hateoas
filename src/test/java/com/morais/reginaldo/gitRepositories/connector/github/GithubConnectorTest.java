package com.morais.reginaldo.gitRepositories.connector.github;

import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubRepository;
import com.morais.reginaldo.gitRepositories.connector.github.representation.GithubResponse;
import com.morais.reginaldo.gitRepositories.exception.GitConnectorException;
import com.morais.reginaldo.gitRepositories.properties.GitRepositoriesConfigurationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class GithubConnectorTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private GitRepositoriesConfigurationProperties configurationProperties;

    private GithubConnector githubConnector;

    private String endpoint = "http://localhost:8080/user/";

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        githubConnector = new GithubConnector(restTemplate, configurationProperties);
    }

    @Test
    public void test_get_git_hub_repositories_by_git_username_should_return_success() throws Exception {

        final String gitUsername = "john.doe";
        GithubRepository[] githubRepositories = buildGithubRepositories();
        ResponseEntity<GithubRepository[]> responseEntity = new ResponseEntity<GithubRepository[]>(githubRepositories, HttpStatus.OK);
        GithubResponse githubResponse = new GithubResponse(new ArrayList<>(Arrays.asList(githubRepositories)));

        when(configurationProperties.getUrl()).thenReturn(endpoint);
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<GithubRepository[]>>any()
        )).thenReturn(responseEntity);

        GithubResponse githubResponseResponse = githubConnector.get(gitUsername);

        assertEquals(githubResponse, githubResponseResponse);
    }

    @Test
    public void test_get_git_hub_repositories_by_git_username_should_return_exception() throws Exception {

        assertThrows(GitConnectorException.class, () -> {
            final String gitUsername = "john.doe";

            when(configurationProperties.getUrl()).thenReturn(endpoint);
            when(restTemplate.exchange(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.any(HttpMethod.class),
                    ArgumentMatchers.any(),
                    ArgumentMatchers.<Class<ResponseEntity<GithubRepository[]>>>any()
            )).thenThrow(new GitConnectorException());

            githubConnector.get(gitUsername);
        });
    }

    private GithubRepository[] buildGithubRepositories() {

        GithubRepository githubRepository = new GithubRepository(1, "some-uri", "repo-name", "some-description", false);
        GithubRepository githubRepositoryTwo = new GithubRepository(1, "some-uri", "repo-name", "some-description", false);

        GithubRepository[] githubRepositories = new GithubRepository[2];
        githubRepositories[0] = githubRepository;
        githubRepositories[1] = githubRepositoryTwo;

        return githubRepositories;
    }
}
package com.morais.reginaldo.health.check;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.base.MockitoException;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RestTemplateHealthIndicatorTest {

    @Mock
    private RestTemplate restTemplate;

    private RestTemplateHealthIndicator restTemplateHealthIndicator;

    @BeforeEach
    public void before() throws Exception {

        MockitoAnnotations.initMocks(this);
        restTemplateHealthIndicator = new RestTemplateHealthIndicator(restTemplate);
    }

    @Test
    public void test_check_health_of_rest_template_should_return_health_up() throws Exception {

        ResponseEntity<String> responseEntity = new ResponseEntity<String>("ok", HttpStatus.OK);

        when(restTemplate.exchange(
                ArgumentMatchers.<RequestEntity>any(),
                ArgumentMatchers.<Class<String>>any()
        )).thenReturn(responseEntity);

        Health healthResult = restTemplateHealthIndicator.health();

        assertEquals(Status.UP, healthResult.getStatus());
    }

    @Test
    public void test_check_health_of_rest_template_with_some_http_error_should_return_health_down() throws Exception {

        ResponseEntity<String> responseEntity =
                new ResponseEntity<String>("Some Error", HttpStatus.INTERNAL_SERVER_ERROR);

        when(restTemplate.exchange(
                ArgumentMatchers.<RequestEntity>any(),
                ArgumentMatchers.<Class<String>>any()
        )).thenReturn(responseEntity);

        Health healthResult = restTemplateHealthIndicator.health();

        assertEquals(Status.DOWN, healthResult.getStatus());
    }

    @Test
    public void test_check_health_of_rest_template_with_some_exception_should_return_health_down() throws Exception {

        when(restTemplate.exchange(
                ArgumentMatchers.<RequestEntity>any(),
                ArgumentMatchers.<Class<String>>any()
        )).thenThrow(new MockitoException("Some Error"));

        Health healthResult = restTemplateHealthIndicator.health();

        assertEquals(Status.DOWN, healthResult.getStatus());
    }
}